# Use the Python3.7.2 image
FROM python:3.7.2-stretch

RUN apt-get update -y
RUN python3 -m pip install --upgrade pip
RUN apt-get install tesseract-ocr-ara
RUN apt-get update ##[edited]
RUN apt-get install 'ffmpeg'\
    'libsm6'\ 
    'libxext6'  

#RUN apt-get install tesseract-ocr libtesseract-dev libleptonica-dev pkg-config -y
#RUN apt-get install python-wheel -y
#RUN apt-get install cmake -y

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app 
ADD . /app

# Install the dependencies
RUN pip3 install -r requirements.txt
EXPOSE 8000

# run the command to start uWSGI
CMD ["python3", "run.py"]
