from collections import namedtuple
import pytesseract
from pytesseract import Output
import cv2
import imutils
import random
#import os

class textLocalisationOCR():
    def cleanup_text(self, text):
        return "".join([c if ord(c)<123 else "" for c in text]).strip()

    def text_localization(self, language, image, inferencePath):
        lang = language
        file_path = inferencePath
        text_dict = pytesseract.image_to_data(image, lang=lang, output_type=Output.DICT)
        for i in range(0, len(text_dict["text"])):
            x = text_dict["left"][i]
            y = text_dict["top"][i]
            w = text_dict["width"][i]
            h = text_dict["height"][i]
            res = text_dict["text"][i]
            conf = int(text_dict["conf"][i])
            if conf > 50:
                cv2.rectangle(image, (x,y), (x+w, y+h), (0,255,0), 2)
        
        cv2.imwrite(file_path+'.jpg', image)
        print("[Inference]: Text Localised image is saved in path {}.jpg".format(file_path))

    def text_detection(self, language, imagePath, inferencePath, textLocalization:bool):
        filename = os.path.basename(imagePath).split('.')[0]
        file_path = os.path.join(inferencePath, filename)
        lang = language
        image = cv2.imread(imagePath)
        rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        options = "-l {} --psm {}".format(lang, 3)
        
        text_string = pytesseract.image_to_string(rgb,config=options)
        if lang == 'eng':
            text_string = self.cleanup_text(text_string)
        txt_file = file_path+'.txt'
        with open(txt_file, 'w', encoding='utf-8') as txtwriter:
            txtwriter.writelines(text_string)
        
        if textLocalization is True:
            self.text_localization(lang, rgb, file_path)        
        print("[Inference]: OCR data has been written in location {}".format(txt_file))








        






    


