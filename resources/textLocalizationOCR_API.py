#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
__author__      = "Geet Tapan Telang"
__version__     = "0.1.1"
__copyright__   = "Copyright 2020, Project SDP"
__maintainer__  = "Geet Tapan Telang"
__status__      = "Development"
__description__      = "An API called /api/OCRDetection which will be used to 
extract text from images"
__api__ = "http://127.0.0.1:5252/api/OCRDetection"

""" 
import logging
import os
from flask_restful import Resource, reqparse, request
from resources.textLocalizationOCR import textLocalisationOCR
import threading

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter("%(levelname)s:%(asctime)s:%(name)s:%(message)s")
file_handler = logging.FileHandler('ocrDetection.log', mode='w')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)

class textLocalizationOCR_API(Resource):
    def post(self):
        data = request.form
        try:
            language = data["Language"]
            imagePath = data["image_location"]
            inferencePath = data["save_location"]
            if data["localize_text"].lower() == 'true':
                textLocalisation = True
            else:
                textLocalisation = False
        except Exception as e:
            logger.error("Input parameters are not provided correct. {}".format(e))
            return e, 401
        analysis_thread = threading.Thread(target=textLocalisationOCR().text_detection, args=(language, imagePath, inferencePath, textLocalisation))
        analysis_thread.start()
        return "Analysis Started", 200
