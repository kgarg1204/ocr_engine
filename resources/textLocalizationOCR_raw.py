from collections import namedtuple
import pytesseract
from pytesseract import Output
import cv2
import imutils
import random
import os

# path = "/Users/geet/Desktop"
# image_path = "/Users/geet/02_Development/06_Proven_Development/InvoiceDetection/images_to_test/basic-invoice.jpg"
# filename = os.path.basename(image_path).split('.')[0]
# file_path = os.path.join(path, filename)
# image = cv2.imread(image_path)
# rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

class textLocalisationOCR():
    def cleanup_text(self, text):
        return "".join([c if ord(c)<123 else "" for c in text]).strip()

    def text_localization(self, language, image, inferencePath):
        lang = language
        file_path = inferencePath
        text_dict = pytesseract.image_to_data(image, lang=lang, output_type=Output.DICT)
        print(text_dict["text"])
        for i in range(0, len(text_dict["text"])):
            x = text_dict["left"][i]
            y = text_dict["top"][i]
            w = text_dict["width"][i]
            h = text_dict["height"][i]
            res = text_dict["text"][i]
            conf = int(text_dict["conf"][i])
            if conf > 50:
                # print("Confidence: {}".format(conf))
                # print("Text: {}". format(res))
                # print("")
                cv2.rectangle(image, (x,y), (x+w, y+h), (0,255,0), 2)
                # cv2.putText(image, res, (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1)
        cv2.imwrite(file_path+'.jpg', image)
        print("[Inference]: Text Localised image is saved in path {}.jpg".format(file_path))

    def text_detection(self, language, imagePath, inferencePath, textLocalization:bool):
        filename = os.path.basename(imagePath).split('.')[0]
        file_path = os.path.join(inferencePath, filename)
        lang = language
        image = cv2.imread(imagePath)
        rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        options = "-l {} --psm {}".format(lang, 3)
        
        text_string = pytesseract.image_to_string(rgb,config=options)
        text_string = self.cleanup_text(text_string)
        txt_file = file_path+'.txt'
        with open(txt_file, 'w') as txtwriter:
            txtwriter.writelines(text_string)
        
        if textLocalization is True:
            self.text_localization(lang, rgb, file_path)
        
        print("[Inference]: OCR data has been written in location {}".format(txt_file))


# ocr_detection = textLocalisationOCR()
# image_directory = "/Users/geet/02_Development/06_Proven_Development/InvoiceDetection/images_to_test/basic-invoice.jpg"
# save_directory = "/Users/geet/Desktop"
# ocr_detection.text_detection('ara', image_directory, save_directory, textLocalization=True)







        






    


