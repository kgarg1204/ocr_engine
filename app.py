#main
from flask import Blueprint
from flask_restful import Api
from flask_cors import CORS

from resources.textLocalizationOCR_API import textLocalizationOCR_API

api_bp = Blueprint('api', __name__)
CORS(api_bp)

api = Api(api_bp)

api.add_resource(textLocalizationOCR_API, '/OCRDetection')
