from flask import Flask, Response
from flask_restful import Resource, request
from threading import Thread
from time import sleep
from app import api_bp
import config

app = Flask(__name__)
app.register_blueprint(api_bp, url_prefix='/api')

if __name__=="__main__":
    app.run(debug=False, threaded=True, host=config.HOST, port=config.PORT)
